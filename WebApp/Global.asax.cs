﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public List<(string name, string dateOfPublish, string text)> articles =
            new List<(string name, string dateOfPublish, string text)>()
            {
                ("Sherlock Holmes Adventure",
                DateTime.Now.ToShortDateString(),
                @"The name of Sherlock Holmes is known to everyone, without exception.
                Arthur Conan Doyle created the image of a noble, fair and insightful detective.
                His novels about a detective have become classics of world literature, 
                and Sherlock himself is a favorite character of several generations of readers. 
                For loyal fans of the detective genre, 
                we have collected a collection of books about the legendary hero."),

                ("Gulliver's Travels",
                DateTime.Now.ToShortDateString(),
                @"A novel that paved the way for the authors of many fantastic trends - from satire to alternative geography.
                And what a detailed construction of worlds is worth! 
                Gulliver's Travels cannot be squeezed only onto a fantastic shelf - it is a phenomenon of universal human culture."),

                ("A Brief History of Japan",
                DateTime.Now.ToShortDateString(),
                @"An updated and enlarged edition of the book, 
                which since its first publication in English 
                has become one of the fundamental books in world Japanese studies.")
            };

        public List<(string name, string dateOfPublish, string text)> reviews =
            new List<(string name, string dateOfPublish, string text)>()
            {
                ("Ray", DateTime.Now.ToShortDateString(), "A very addicting detective story"),
                ("Tom", DateTime.Now.ToShortDateString(), "I give 5 stars this book"),
                ("John", DateTime.Now.ToShortDateString(), " Japan is amazing")
            };

        public List<(string name, string genre, bool wantScifi,
                bool wantMobileVersion)> forms = new List<(string name,
                string genre, bool wantScifi, bool wantMobileVersion)>();

        protected void Application_Start()
        {
            Application["articles"] = articles;
            Application["reviews"] = reviews;
            Application["forms"] = forms;


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }

}
